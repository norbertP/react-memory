import React from 'react'
import { shallow } from 'enzyme'
import Card from './Card'
import { expect } from 'chai'
import sinon from 'sinon'
import App, { SYMBOLS } from './App';

describe('<Card/>', () => {
    const mock = sinon
    beforeAll(()=>{
        mock
        .stub(App.prototype, 'generateCards')
        .returns([...SYMBOLS.repeat(2)])
    const wrapper = shallow(<App />)
    expect(wrapper).to.matchSnapshot()}
)

it('should trigger its onClick prop when clicked', () => {
    const onClick = sinon.spy()
    const wrapper = shallow(
        <Card
            card='😀'
            feedback='hidden'
            index={0}
            onClick={onClick}
        />
    )
    wrapper.simulate('click')
    expect(onClick).to.have.been.calledWith(0)
})
it('should fill the currentPair when a card is clicked', () => {
    const wrapper = shallow(<App />)
    wrapper.find('Card').at(0).shallow().simulate('click')
    expect(wrapper.state('currentPair')[0]).to.equal(0)
})
it('should fill the currentPair completed when a second card is clicked', () => {
    const wrapper = shallow(<App />)
    wrapper.find('Card').at(0).shallow().simulate('click')
    expect(wrapper.state('currentPair')[0]).to.equal(0)
    wrapper.find('Card').at(1).shallow().simulate('click')
    expect(wrapper.state('currentPair')[1]).to.equal(1)
})
it('should matched and fill matchedCardIndices', () => {
    const wrapper = shallow(<App />)
    wrapper.find('Card').at(0).shallow().simulate('click')
    expect(wrapper.state('currentPair')[0]).to.equal(0)
    wrapper.find('Card').at(18).shallow().simulate('click')
    expect(wrapper.state('currentPair')[1]).to.equal(18)
    expect(wrapper.state('matchedCardIndices')).to.eql([0, 18])

})

it('should not be able to click on 3 cards', () => {
    const wrapper = shallow(<App />)
    wrapper.find('Card').at(0).shallow().simulate('click')
    expect(wrapper.state('currentPair')[0]).to.equal(0)
    wrapper.find('Card').at(16).shallow().simulate('click')
    expect(wrapper.state('currentPair')[1]).to.equal(16)
    wrapper.find('Card').at(17).shallow().simulate('click')
    expect(wrapper.state('currentPair')).to.not.have.length(3)
})

it('should match its reference snapshot', () => {
    const onClick = sinon.spy()
    const wrapper = shallow(
        <Card
            card='😀'
            feedback='hidden'
            index={0}
            onClick={onClick}
        />
    )
    expect(wrapper).to.matchSnapshot()
})

afterAll(()=>{
    mock.restore()
})
})
